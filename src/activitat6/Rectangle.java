package activitat6;

public class Rectangle {

    private int ample;
    private int altura;

    public Rectangle(int ample, int altura) {
        this.ample = ample;
        this.altura = altura;
    }

    public int calcularArea() {
        return this.ample * this.altura;
    }

    public void setAmple(int ample) {
        this.ample = ample;
    }

    public int getAmple() {
        return ample;
    }

    public int getAltura() {
        return altura;
    }

    public void mostrarInfo() {
        System.out.println("Altura: " + this.altura);
        System.out.println("Anchura: " + this.ample);
        System.out.println("Área: " + this.calcularArea());
    }
}
