package activitat5;

public class TestPersona {

    public static void main(String[] args) {

        Persona persona1 = new Persona(
                "Angustias",
                "Matutinas", "Solitarias",
                56, false);

        Persona persona2 = new Persona(
                "Arturito",
                "Turi", "Turi",
                32);

        persona1.saluda();
        persona2.saluda();
        
        persona2.cambiaEstadoCivil();
        persona2.saluda();

    }
}
