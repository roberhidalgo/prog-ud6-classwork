package activitat5;

public class Persona {

    private String nom;
    private String cognom1;
    private String cognom2;
    private int edat;
    private boolean estaCasat;

    public Persona(String nom,
                   String cognom1,
                   String cognom2,
                   int edat, boolean estaCasat) {
        this.nom = nom;
        this.cognom1 = cognom1;
        this.cognom2 = cognom2;
        this.edat = edat;
        this.estaCasat = estaCasat;
    }

    public Persona(String nom,
                   String cognom1, String cognom2,
                   int edat) {
        this.nom = nom;
        this.cognom1 = cognom1;
        this.cognom2 = cognom2;
        this.edat = edat;
        this.estaCasat = false;
    }

    public void saluda() {

        System.out.println("Hola, sóc " + this.nom + " " + this.cognom1 + " "
                + this.cognom2 + ", la meva edat és " + this.edat
                + " anys i estic " + this.estadoCivil());
    }

    private String estadoCivil() {
        if (this.estaCasat) {
            return "casat";
        }

        return "solter";
    }

    public void cambiaEstadoCivil() {

        this.estaCasat = !this.estaCasat;
    }

}
