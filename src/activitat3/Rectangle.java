package activitat3;

public class Rectangle {

    private int ample;
    private int altura;

    public Rectangle(int ample, int altura) {
        this.ample = ample;
        this.altura = altura;
    }

    public int calcularArea() {

        return this.ample * this.altura;
    }
}
