package activitat3;

public class TestRectangle {

    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(100, 200);
        System.out.println("El área de r1 es " + r1.calcularArea());

        Rectangle r2 = new Rectangle(40, 100);
        System.out.println("El área de r2 es " + r2.calcularArea());
        Rectangle r3 = new Rectangle(80, 43);
        System.out.println("El área de r3 es " + r3.calcularArea());
        Rectangle r4 = new Rectangle(56, 90);
        System.out.println("El área de r1 es " + r4.calcularArea());
        Rectangle r5 = new Rectangle(100, 150);
        System.out.println("El área de r1 es " + r5.calcularArea());
        Rectangle r6 = new Rectangle(200, 120);
        System.out.println("El área de r1 es " + r6.calcularArea());
    }
}
